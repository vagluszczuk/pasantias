
function postData() {
    const form = document.querySelector('form');
    const formData = new FormData(form);
    const xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function() {
      if (this.readyState === 4 && this.status === 200) {
        const divPrecioDolar = document.getElementById('div_precio_dolar');
        const data = JSON.parse(this.responseText);
        const precioDolar = data[0].dolar;
        divPrecioDolar.textContent = `Precio del dólar: ${precioDolar}`;
      }
    };
    xhr.open('POST', form.action, true);
    xhr.send(formData);
  }


function ver(n) {
    document.getElementById("form"+n).style.display = "block"
  }
  function ocultar(n) {
    document.getElementById("form"+n).style.display = "none"
  }

  $(document).ready(function() {
    // Capturar el evento de envío del formulario
    $("#form_stock").submit(function(event) {
        event.preventDefault(); // Evitar el envío del formulario

        // Obtener los datos del formulario
        var formData = $(this).serialize();

        // Enviar los datos al servidor mediante AJAX
        $.ajax({
            url: "../php/cambiar_stock_disp.php",
            type: "POST",
            data: formData,
            success: function(response) {
                // Procesar la respuesta del servidor
                console.log(response);
                // Realizar otras operaciones necesarias

                // Por ejemplo, mostrar un mensaje de éxito
                alert("Stock cambiado correctamente");

                $("#form_stock")[0].reset();
            },
            error: function(xhr, status, error) {
                if (xhr.status == 400) {
                    alert("rellene ambos campos");
                } else if (xhr.status == 500) {
                    alert("Error en el cambio de stock.");
                } else {
                    alert("Ha ocurrido un error inesperado");
                }
            }
        });
    });
});

$(document).ready(function() {
    // Capturar el evento de envío del formulario
    $("#form_disp").submit(function(event) {
        event.preventDefault(); // Evitar el envío del formulario

        // Obtener los datos del formulario
        var formData = $(this).serialize();

        // Enviar los datos al servidor mediante AJAX
        $.ajax({
            url: "../php/cambiar_stock_disp.php",
            type: "POST",
            data: formData,
            success: function(response) {
                // Procesar la respuesta del servidor
                console.log(response);
                // Realizar otras operaciones necesarias

                // Por ejemplo, mostrar un mensaje de éxito
                alert("Disponibilidad cambiada correctamente");

                $("#form_disp")[0].reset();
            },
            error: function(xhr, status, error) {
                if (xhr.status == 400) {
                    alert("rellene ambos campos");
                } else if (xhr.status == 500) {
                    alert("Error en el cambio de diponibilidad.");
                } else {
                    alert("Ha ocurrido un error inesperado");
                }
            }
        });
    });
});

$(document).ready(function() {
    // Capturar el evento de envío del formulario
    $("#form_precios").submit(function(event) {
        event.preventDefault(); // Evitar el envío del formulario

        // Obtener los datos del formulario
        var formData = $(this).serialize();

        // Enviar los datos al servidor mediante AJAX
        $.ajax({
            url: "../php/cambiar_precios_productos.php",
            type: "POST",
            data: formData,
            success: function(response) {
                // Procesar la respuesta del servidor
                console.log(response);
                // Realizar otras operaciones necesarias
                postData()
                // Por ejemplo, mostrar un mensaje de éxito
                alert("precios cambiados correctamente");

                $("#form_precios")[0].reset();
            },
            error: function(xhr, status, error) {
                if (xhr.status == 400) {
                    alert("rellene solo un campo a la vez");
                } else if (xhr.status == 500) {
                    alert("Error en el cambio de precios.");
                } else {
                    alert("Ha ocurrido un error inesperado");
                }
            }
        });
    });
});

function mostrarConfirmacion() {
    alert("Cambios realizados con éxito");
    // $("#form_in_productos")[0].reset();
    // $("#form_img")[0].reset();
  }

  window.onload = function() {
    // Obtén una referencia al botón mediante su ID
    var boton = document.getElementById('btn_dolar');

    // Simula un clic en el botón
    boton.click();
  };

  function mostrar() {
    const form = document.querySelector('#edicion_pre');
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      console.log('readyState:', this.readyState);
      console.log('status:', this.status);
    
      if (this.readyState === 4 && this.status === 200) {
        const datos_cod = document.getElementById('form6');
        datos_cod.innerHTML = '';
        const data = JSON.parse(this.responseText);
        if (data.length > 0) {
          const tableContainer = document.createElement('div');
          tableContainer.className = 'table-container'; // Agrega una clase CSS para el contenedor
          const table = document.createElement('table');
          const thead = document.createElement('thead');
          const tbody = document.createElement('tbody');
          const headerRow = document.createElement('tr');
          const headers = ['Nombre', 'Imagen', 'Precio'];
          headers.forEach(headerText => {
            const header = document.createElement('th');
            const textNode = document.createTextNode(headerText);
            header.appendChild(textNode);
            headerRow.appendChild(header);
          });
          thead.appendChild(headerRow);
          data.forEach(rowData => {
            const row = document.createElement('tr');
          
            // create cells for each column and add them to the row
            const cellNombre = document.createElement('td');
            cellNombre.textContent = rowData.nombre;
            row.appendChild(cellNombre);
          
            const cellImagen = document.createElement('td');
            // create an image element and set its properties
            const imagen = document.createElement('img');
            imagen.src = rowData.imagen;
            imagen.alt = rowData.nombre;
            imagen.style.maxWidth = '150px';
            imagen.style.maxHeight = '150px';
            cellImagen.appendChild(imagen);
            row.appendChild(cellImagen);
          
            const cellPrecio = document.createElement('td');
            cellPrecio.textContent = rowData.precio;
            row.appendChild(cellPrecio);
          
            // add the row to the table body
            tbody.appendChild(row);
          });
          table.appendChild(thead);
        table.appendChild(tbody);
        datos_cod.appendChild(table);
        } else {
            datos_cod.textContent = 'No se encontraron resultados para la fecha seleccionada.';
        }
      }
    };
    
    const codProducto = document.querySelector('input[name="cod_producto"]').value;
    const dataString = 'cod_producto=' + codProducto;
    
    xhr.open('POST', '../php/datos_productos_edicion.php', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(dataString);
  }

  function mostrar1() {
    const form = document.querySelector('#form_img');
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      console.log('readyState:', this.readyState);
      console.log('status:', this.status);
    
      if (this.readyState === 4 && this.status === 200) {
        const datos_cod = document.getElementById('form6');
        datos_cod.innerHTML = '';
        const data = JSON.parse(this.responseText);
        if (data.length > 0) {
          const tableContainer = document.createElement('div');
          tableContainer.className = 'table-container'; // Agrega una clase CSS para el contenedor
          const table = document.createElement('table');
          const thead = document.createElement('thead');
          const tbody = document.createElement('tbody');
          const headerRow = document.createElement('tr');
          const headers = ['Nombre', 'Imagen', 'Precio'];
          headers.forEach(headerText => {
            const header = document.createElement('th');
            const textNode = document.createTextNode(headerText);
            header.appendChild(textNode);
            headerRow.appendChild(header);
          });
          thead.appendChild(headerRow);
          data.forEach(rowData => {
            const row = document.createElement('tr');
          
            // create cells for each column and add them to the row
            const cellNombre = document.createElement('td');
            cellNombre.textContent = rowData.nombre;
            row.appendChild(cellNombre);
          
            const cellImagen = document.createElement('td');
            // create an image element and set its properties
            const imagen = document.createElement('img');
            imagen.src = rowData.imagen;
            imagen.alt = rowData.nombre;
            imagen.style.maxWidth = '150px';
            imagen.style.maxHeight = '150px';
            cellImagen.appendChild(imagen);
            row.appendChild(cellImagen);
          
            const cellPrecio = document.createElement('td');
            cellPrecio.textContent = rowData.precio;
            row.appendChild(cellPrecio);
          
            // add the row to the table body
            tbody.appendChild(row);
          });
          table.appendChild(thead);
        table.appendChild(tbody);
        datos_cod.appendChild(table);
        } else {
            datos_cod.textContent = 'No se encontraron resultados para la fecha seleccionada.';
        }
      }
    };
    
    const codProducto = document.querySelector('input[name="cod_producto_img"]').value;
    const dataString = 'cod_producto=' + codProducto;
    
    xhr.open('POST', '../php/datos_productos_edicion.php', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send(dataString);
  }

const input = document.getElementById('cod_producto_1');
const input1 = document.getElementById('cod_producto_img');
input.addEventListener('input', function() {
  mostrar();
});
input1.addEventListener('input', function() {
  mostrar1();
});



// // Get the input element
// const inputCodProducto = document.getElementById('cod_producto_1');

// // Add an event listener for the input event
// inputCodProducto.addEventListener('input', function() {
//   // Get the value of the input
//   const codProducto = inputCodProducto.value;

//   // Make an AJAX request to the PHP file
//   $.ajax({
//     url: '../php/datos_productos_edicion.php',
//     type: 'POST',
//     data: {cod_producto:codProducto},
//     success: function(response) {
//       // Check if the response is a JSON string
//       if (1==1) {
//         // Parse the response as JSON
//         const data = JSON.parse(this.responseText);

//         // Create an HTML table with the desired columns and rows
//         let tableHtml = '<table>';
//         tableHtml += '<tr><th>Nombre</th><th>Imagen</th><th>Precio</th><th>Descripción</th></tr>';
//         data.forEach(function(row) {
//           tableHtml += '<tr>';
//           tableHtml += '<td>' + row.nombre + '</td>';
//           tableHtml += '<td><img src="' + row.imagen + '"></td>';
//           tableHtml += '<td>' + row.precio + '</td>';
//           tableHtml += '<td>' + row.descripcion + '</td>';
//           tableHtml += '</tr>';
//         });
//         tableHtml += '</table>';

//         // Show the table in the div with the id 'table_div'
//         const tableDiv = document.getElementById('datos_cod');
//         tableDiv.innerHTML = tableHtml;
//       } else {
//         // Handle the error
//         console.log('Response is not a JSON string');
//       }
//     },
//     error: function(xhr, status, error) {
//       // Handle the error
//       console.log(error);
//     }
//   });
// });