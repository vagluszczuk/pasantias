function llenarDesplegableProductos(opcionesProductoId) {
  fetch('../php/mostrar_lista_productos.php')
    .then(response => response.json())
    .then(data => {
      var desplegableProductos = document.getElementById(opcionesProductoId);
      data.forEach(dato => {
        var option = document.createElement('option');
        option.value = dato.nombreCodigo;
        option.text =  dato.nombreCodigo + ' | '+ 'stock: ' + dato.stock;
        desplegableProductos.appendChild(option);
      });
    })
    .catch(error => console.error('Error:', error));
}
function llenarDesplegableProductosAll(opcionesProductoId) {
  fetch('../php/mostrar_lista_productos_all.php')
    .then(response => response.json())
    .then(data => {
      var desplegableProductos = document.getElementById(opcionesProductoId);
      data.forEach(dato => {
        var option = document.createElement('option');
        option.value = dato;
        option.text = dato;
        desplegableProductos.appendChild(option);
      });
    })
    .catch(error => console.error('Error:', error));
}

function llenarDesplegableClientes() {
  fetch('../php/mostrar_lista_clientes.php')
    .then(response => response.json())
    .then(data => {
      var desplegable = document.getElementById('opciones_cliente');
      data.forEach(dato => {
        var option = document.createElement('option');
        option.value = dato.id_cliente;
        option.text = dato.nombre + ' ' + dato.apellido;
        desplegable.appendChild(option);
      });
    })
    .catch(error => console.error('Error:', error));
}

function llenarDesplegableVendedores() {
  fetch('../php/mostrar_lista_vendedores.php')
    .then(response => response.json())
    .then(data => {
      var desplegableClientes = document.getElementById('opciones_vendedor');
      data.forEach(dato => {
        var option = document.createElement('option');
        option.value = dato.id_vendedor;
        option.text = dato.nombre + ' ' + dato.apellido;
        desplegableClientes.appendChild(option);
      });
    })
    .catch(error => console.error('Error:', error));
}

window.onload = function() {
  llenarDesplegableProductos('opciones_producto');
    llenarDesplegableProductosAll('opciones_producto_all');
    llenarDesplegableClientes();
    llenarDesplegableVendedores();
};

var contador=0;

function agregarElemento() {
  contador++;

  var productoSeleccionado = document.getElementById('opciones_producto').value;
  var cantidadSeleccionada = document.getElementById('cantidad').value;

  if (productoSeleccionado && cantidadSeleccionada) {
    // Obtener el precio del producto mediante una petición AJAX
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "../php/obtener_precio.php?producto=" + encodeURIComponent(productoSeleccionado), true);
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          var precio = xhr.responseText;
          var subtotal = parseFloat(precio) * parseFloat(cantidadSeleccionada);

          // Mostrar los datos en el carrito
          var carrito = document.getElementById('carrito').getElementsByTagName('tbody')[0];
          var fila = document.createElement('tr');
          var celdaProducto = document.createElement('td');
          var celdaPrecio = document.createElement('td');
          var celdaCantidad = document.createElement('td');
          var celdaSubtotal = document.createElement('td');
          var celdaEliminar = document.createElement('td');
          var botonEliminar = document.createElement('button');

          celdaProducto.textContent = productoSeleccionado;
          celdaPrecio.textContent = cantidadSeleccionada;
          celdaCantidad.textContent = precio;
          celdaSubtotal.textContent = subtotal.toFixed(2); // Redondear el subtotal a 2 decimales

          botonEliminar.textContent = "X";
          botonEliminar.addEventListener('click', function() {
            carrito.removeChild(fila); // Eliminar la fila del carrito al hacer clic en el botón

            // Eliminar el elemento del array
            var datosArrayCompleto = obtenerDatosArray();
            var indice = datosArrayCompleto.findIndex(function(elemento) {
              return elemento.producto === productoSeleccionado && elemento.cantidad === cantidadSeleccionada && elemento.precio === precio && elemento.subtotal === subtotal.toFixed(2);
            });

            if (indice !== -1) {
              datosArrayCompleto.splice(indice, 1);
            }

            // Guardar el array actualizado en el almacenamiento local
            localStorage.setItem('datosArray', JSON.stringify(datosArrayCompleto));
          });

          celdaEliminar.appendChild(botonEliminar);

          fila.appendChild(celdaProducto);
          fila.appendChild(celdaPrecio);
          fila.appendChild(celdaCantidad);
          fila.appendChild(celdaSubtotal);
          fila.appendChild(celdaEliminar);
          carrito.appendChild(fila);

          // Limpiar los campos de selección
          document.getElementById('cantidad').value = '';

          // Agregar los datos al array
          var datosArray = {
            producto: productoSeleccionado,
            cantidad: cantidadSeleccionada,
            precio: precio,
            subtotal: subtotal.toFixed(2)
          };

          // Obtener el array de datos existente o crear uno nuevo
          var datosArrayCompleto = obtenerDatosArray();

          // Agregar el nuevo objeto al array
          datosArrayCompleto.push(datosArray);

          // Guardar el array actualizado en el almacenamiento local
          localStorage.setItem('datosArray', JSON.stringify(datosArrayCompleto));
        } else {
          console.error('Error en la petición AJAX:', xhr.status);
        }
      }
    };
    xhr.send();
  }
}

function obtenerDatosArray() {
  var datosArrayExistente = localStorage.getItem('datosArray');
  var datosArrayCompleto = [];

  if (datosArrayExistente) {
    datosArrayCompleto = JSON.parse(datosArrayExistente);
  }

  return datosArrayCompleto;
}

function agregarElemento1() {
  contador++;

  var productoSeleccionado = document.getElementById('opciones_producto_all').value;
  var cantidadSeleccionada = document.getElementById('cantidad1').value;

  if (productoSeleccionado && cantidadSeleccionada) {
    // Obtener el precio del producto mediante una petición AJAX
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "../php/obtener_precio.php?producto=" + encodeURIComponent(productoSeleccionado), true);
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          var precio = xhr.responseText;
          var subtotal = parseFloat(precio) * parseFloat(cantidadSeleccionada);

          // Mostrar los datos en el carrito
          var carrito = document.getElementById('carrito1').getElementsByTagName('tbody')[0];
          var fila = document.createElement('tr');
          var celdaProducto = document.createElement('td');
          var celdaPrecio = document.createElement('td');
          var celdaCantidad = document.createElement('td');
          var celdaSubtotal = document.createElement('td');
          var celdaEliminar = document.createElement('td');
          var botonEliminar = document.createElement('button');

          celdaProducto.textContent = productoSeleccionado;
          celdaPrecio.textContent = cantidadSeleccionada;
          celdaCantidad.textContent = precio;
          celdaSubtotal.textContent = subtotal.toFixed(2); // Redondear el subtotal a 2 decimales

          botonEliminar.textContent = "X";
          botonEliminar.addEventListener('click', function() {
            carrito.removeChild(fila); // Eliminar la fila del carrito al hacer clic en el botón

            // Eliminar el elemento del array
            var datosArrayCompleto = obtenerDatosArray();
            var indice = datosArrayCompleto.findIndex(function(elemento) {
              return elemento.producto === productoSeleccionado && elemento.cantidad === cantidadSeleccionada && elemento.precio === precio && elemento.subtotal === subtotal.toFixed(2);
            });

            if (indice !== -1) {
              datosArrayCompleto.splice(indice, 1);
            }

            // Guardar el array actualizado en el almacenamiento local
            localStorage.setItem('datosArray', JSON.stringify(datosArrayCompleto));
          });

          celdaEliminar.appendChild(botonEliminar);

          fila.appendChild(celdaProducto);
          fila.appendChild(celdaPrecio);
          fila.appendChild(celdaCantidad);
          fila.appendChild(celdaSubtotal);
          fila.appendChild(celdaEliminar);
          carrito.appendChild(fila);

          // Limpiar los campos de selección
          document.getElementById('cantidad').value = '';

          // Agregar los datos al array
          var datosArray = {
            producto: productoSeleccionado,
            cantidad: cantidadSeleccionada,
            precio: precio,
            subtotal: subtotal.toFixed(2)
          };

          // Obtener el array de datos existente o crear uno nuevo
          var datosArrayCompleto = obtenerDatosArray();

          // Agregar el nuevo objeto al array
          datosArrayCompleto.push(datosArray);

          // Guardar el array actualizado en el almacenamiento local
          localStorage.setItem('datosArray', JSON.stringify(datosArrayCompleto));
        } else {
          console.error('Error en la petición AJAX:', xhr.status);
        }
      }
    };
    xhr.send();
  }
}

function obtenerDatosArray() {
  var datosArrayExistente = localStorage.getItem('datosArray');
  var datosArrayCompleto = [];

  if (datosArrayExistente) {
    datosArrayCompleto = JSON.parse(datosArrayExistente);
  }

  return datosArrayCompleto;
}


// Función para gestionar el envío del formulario
function enviarFormulario() {
  // Obtener los datos del formulario
  var formulario = document.querySelector('#form_venta');
  var formData = new FormData(formulario);

  // Obtener el array de datos del almacenamiento local
  var datosArrayExistente = localStorage.getItem('datosArray');

  if (datosArrayExistente) {
    // Agregar el array de datos al formulario
    formData.append('datosArray', datosArrayExistente);
  }

  formData.append('contador', contador);

  $.ajax({
    url: '../php/guardar_datos_ventas.php',
    type: 'POST', // Establecer el método POST
    data: formData,
    processData: false,
    contentType: false,
    success: function(response) {
      // Manejar la respuesta del servidor si es necesario

      // Limpiar el almacenamiento local
      localStorage.removeItem('datosArray');

      // Abrir el archivo PHP en una nueva pestaña
      // Obtener la URL del archivo PHP
      var urlPHP = '../php/guardar_datos_ventas.php';

      // Crear un formulario oculto
      var hiddenForm = document.createElement('form');
      hiddenForm.setAttribute('method', 'POST');
      hiddenForm.setAttribute('action', urlPHP);
      hiddenForm.setAttribute('target', '_blank'); // Abrir en una nueva pestaña
      hiddenForm.style.display = 'none';

      // Agregar los datos del formulario
      var formDataKeys = formData.keys();
      for (var key of formDataKeys) {
        var inputField = document.createElement('input');
        inputField.setAttribute('type', 'hidden');
        inputField.setAttribute('name', key);
        inputField.setAttribute('value', formData.get(key));
        hiddenForm.appendChild(inputField);
      }

      // Agregar el array de datos al formulario
      var datosArrayExistenteInput = document.createElement('input');
      datosArrayExistenteInput.setAttribute('type', 'hidden');
      datosArrayExistenteInput.setAttribute('name', 'datosArray');
      datosArrayExistenteInput.setAttribute('value', datosArrayExistente);
      hiddenForm.appendChild(datosArrayExistenteInput);

      // Agregar el contador al formulario
      var contadorInput = document.createElement('input');
      contadorInput.setAttribute('type', 'hidden');
      contadorInput.setAttribute('name', 'contador');
      contadorInput.setAttribute('value', contador);
      hiddenForm.appendChild(contadorInput);

      // Agregar el formulario oculto al cuerpo del documento
      document.body.appendChild(hiddenForm);

      // Enviar el formulario
      hiddenForm.submit();
      alertas();

    },
    error: function(xhr, status, error) {
      console.error('Error en la solicitud:', error);
    }
  });
}


function enviarFormulario1() {
  // Obtener los datos del formulario
  var formulario = document.querySelector('#form_presupuesto');
  var formData = new FormData(formulario);

  // Obtener el array de datos del almacenamiento local
  var datosArrayExistente = localStorage.getItem('datosArray');

  if (datosArrayExistente) {
    // Agregar el array de datos al formulario
    formData.append('datosArray', datosArrayExistente);
  }

  formData.append('contador', contador);

  $.ajax({
    url: '../php/presupuesto_de_venta.php',
    type: 'POST', // Establecer el método POST
    data: formData,
    processData: false,
    contentType: false,
    success: function(response) {
      // Manejar la respuesta del servidor si es necesario

      // Limpiar el almacenamiento local
      localStorage.removeItem('datosArray');

      // Abrir el archivo PHP en una nueva pestaña
      // Obtener la URL del archivo PHP
      var urlPHP = '../php/presupuesto_de_venta.php';

      // Crear un formulario oculto
      var hiddenForm = document.createElement('form');
      hiddenForm.setAttribute('method', 'POST');
      hiddenForm.setAttribute('action', urlPHP);
      hiddenForm.setAttribute('target', '_blank'); // Abrir en una nueva pestaña
      hiddenForm.style.display = 'none';

      // Agregar los datos del formulario
      var formDataKeys = formData.keys();
      for (var key of formDataKeys) {
        var inputField = document.createElement('input');
        inputField.setAttribute('type', 'hidden');
        inputField.setAttribute('name', key);
        inputField.setAttribute('value', formData.get(key));
        hiddenForm.appendChild(inputField);
      }

      // Agregar el array de datos al formulario
      var datosArrayExistenteInput = document.createElement('input');
      datosArrayExistenteInput.setAttribute('type', 'hidden');
      datosArrayExistenteInput.setAttribute('name', 'datosArray');
      datosArrayExistenteInput.setAttribute('value', datosArrayExistente);
      hiddenForm.appendChild(datosArrayExistenteInput);

      // Agregar el contador al formulario
      var contadorInput = document.createElement('input');
      contadorInput.setAttribute('type', 'hidden');
      contadorInput.setAttribute('name', 'contador');
      contadorInput.setAttribute('value', contador);
      hiddenForm.appendChild(contadorInput);

      // Agregar el formulario oculto al cuerpo del documento
      document.body.appendChild(hiddenForm);

      // Enviar el formulario
      hiddenForm.submit();


    },
    error: function(xhr, status, error) {
      console.error('Error en la solicitud:', error);
    }
  });
}




// Asignar el evento al botón de enviar
var botonEnviar = document.querySelector('#form_venta button[type="submit"]');
botonEnviar.addEventListener('click', function(event) {
  event.preventDefault();
  enviarFormulario();
});

var botonEnviar = document.querySelector('#form_presupuesto button[type="submit"]');
botonEnviar.addEventListener('click', function(event) {
  event.preventDefault();
  enviarFormulario1();
});

function ver(n) {
  document.getElementById("form"+n).style.display = "block"
}
function ocultar(n) {
  document.getElementById("form"+n).style.display = "none"
}

function postData() {
  const form = document.querySelector('#form_buqueda');
  const formData = new FormData(form);
  const resultados_pdf = document.getElementById('resultados_pdf');
  resultados_pdf.innerHTML = '';

  fetch(form.action, {
    method: 'POST',
    body: formData
  })
  .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Error en la solicitud');
    }
  })
  .then(data => {
    if (data.length > 0) {
      const table = document.createElement('table');
      table.classList.add('tabla_pdf_venta');
      const thead = document.createElement('thead');
      const tbody = document.createElement('tbody');
      const headerRow = document.createElement('tr');
      const headers = ['ID Venta', 'Cliente', 'Vendedor', 'Fecha/Hora'];
      
      headers.forEach(headerText => {
        const header = document.createElement('th');
        const textNode = document.createTextNode(headerText);
        header.appendChild(textNode);
        headerRow.appendChild(header);
      });
      
      thead.appendChild(headerRow);
      
      data.forEach(rowData => {
        const row = document.createElement('tr');
        
        Object.values(rowData).forEach((value, index) => {
          const cell = document.createElement('td');
          const textNode = document.createTextNode(value);
          cell.appendChild(textNode);
          row.appendChild(cell);
        });
        
        const actionsCell = document.createElement('td');
        const button = document.createElement('button');
        button.textContent = 'Mostrar';
        button.addEventListener('click', () => {
          mostrarVenta(rowData.id_venta); // Llamar a la función mostrarVenta con la ID de venta
        });
        actionsCell.appendChild(button);
        row.appendChild(actionsCell);
    
        tbody.appendChild(row);
      });
      
      table.appendChild(thead);
      table.appendChild(tbody);
      resultados_pdf.appendChild(table);
    } else {
      resultados_pdf.textContent = 'No se encontraron resultados para la fecha seleccionada.';
    }
  })
  .catch(error => {
    console.error(error);
    resultados_pdf.textContent = 'No se encontraron resultados para la fecha seleccionada.';
  });
}

function mostrarVenta(idVenta) {
  const url = `../php/mostrar_pdf.php?id_venta=${idVenta}`;
  window.open(url, '_blank');
}

function validarNumeroPositivo(input) {
  if (input.value < 0) {
    input.value = 0;
  }
  if (input.value > 100) {
    input.value = 100;
  }
}

// Obtener el elemento select
const selectProducto = document.getElementById('opciones_producto');

// Agregar un evento de escucha al cambio del select
selectProducto.addEventListener('change', function() {
  // Obtener el valor seleccionado del select
  const selectedValue = this.value;

  // Crear un objeto FormData
  const formData = new FormData();
  formData.append('producto', selectedValue);

  // Realizar una solicitud AJAX al archivo PHP
  fetch('../php/obtener_maximo_stock.php', {
    method: 'POST',
    body: formData
  })
  .then(response => response.json())
  .then(data => {
    // Obtener el elemento input cantidad
    const inputCantidad = document.getElementById('cantidad');

    // Establecer el valor máximo del input cantidad
    inputCantidad.max = data.stock;
  })
  .catch(error => {
    console.error('Error en la solicitud:', error);
  });
});

// Función para validar el valor del input cantidad
function validarstock(input) {
  const stock = parseInt(input.max);
  if (input.value < 0) {
    input.value = 0;
  }
  if (input.value > stock) {
    input.value = stock;
  }
}

// Obtener el elemento input cantidad
const inputCantidad = document.getElementById('cantidad');

// Asignar la función validarstock al evento onchange del input cantidad
inputCantidad.addEventListener('change', function() {
  validarstock(this);
});



function alertas() {
  fetch('../php/verificar_stock.php')
    .then(response => response.json())
    .then(data => {
      displayAlerts(data, 0);
    })
    .catch(error => {
      console.error('Error en la solicitud:', error);
    });
}

function displayAlerts(data, index) {
  if (index >= data.length) {
    return;
  }

  const producto = data[index];

  if (producto.stock <= 0) {
    const confirmation = confirm(`El producto "${producto.nombre}" se quedó sin stock. ¿Desea continuar?`);

    if (confirmation) {
      setTimeout(() => {
        displayAlerts(data, index + 1);
      }, 0);
    } else {
      return;
    }
  } else {
    displayAlerts(data, index + 1);
  }
  localStorage.clear(); // Borrar el localStorage
  window.location.reload();
}