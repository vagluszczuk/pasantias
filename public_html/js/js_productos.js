function postData() {
  const form = document.querySelector('form');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      const resultados_productos = document.getElementById('resultados_productos');
      resultados_productos.innerHTML = '';
      const data = JSON.parse(this.responseText);
      if (data.length > 0) {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['Codigo', 'Nombre', 'Imagen', 'Precio', 'Rubro', 'Descripcion', 'Stock', 'Disponibilidad'];
        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        data.forEach(rowData => {
          const row = document.createElement('tr');

          const cellCodProducto = document.createElement('td');
          cellCodProducto.textContent = rowData.cod_producto;
          row.appendChild(cellCodProducto);

          const cellNombre = document.createElement('td');
          cellNombre.textContent = rowData.nombre;
          row.appendChild(cellNombre);

          const cellImagen = document.createElement('td');
          const imagen = document.createElement('img');
          imagen.src = rowData.imagen;
          imagen.alt = rowData.nombre;
          imagen.style.maxWidth = '150px'; // Establecer el ancho máximo
          imagen.style.maxHeight = '150px'; // Establecer la altura máxima
          cellImagen.appendChild(imagen);
          // Guardar la URL de la imagen en un atributo de datos
          cellImagen.dataset.src = rowData.imagen;
          row.appendChild(cellImagen);

          const cellPrecio = document.createElement('td');
          cellPrecio.textContent = rowData.precio;
          row.appendChild(cellPrecio);

          const cellRubro = document.createElement('td');
          cellRubro.textContent = rowData.rubro;
          row.appendChild(cellRubro);

          const cellDescripcion = document.createElement('td');
          cellDescripcion.textContent = rowData.descripcion;
          row.appendChild(cellDescripcion);

          const cellStock = document.createElement('td');
          cellStock.textContent = rowData.stock;
          row.appendChild(cellStock);

          const cellDisponibilidad = document.createElement('td');
          cellDisponibilidad.textContent = rowData.disponibilidad;
          row.appendChild(cellDisponibilidad);

          const editButtonCell = document.createElement('td');
          const editButton = document.createElement('button');
          editButton.textContent = 'Editar';
          editButton.addEventListener('click', function() {
            const rowCells = this.parentNode.parentNode.childNodes;

            if (this.textContent === 'Editar') {
              const imgCell = rowCells[2];
              const img = imgCell.querySelector('img');
              // Guardar la URL de la imagen antes de convertir la celda en contenido editable
              imgCell.dataset.src = img.src;

              for (let i = 1; i < rowCells.length - 1; i++) {
                const headerText = headers[i];
                if (headerText !== 'Precio') {
                  const cellContent = rowCells[i].textContent;
                  rowCells[i].innerHTML = `<span contenteditable="true">${cellContent}</span>`;
                }
              }
              this.textContent = 'Guardar';
            } else {
              for (let i = 1; i < rowCells.length - 1; i++) {
                const headerText = headers[i];
                if (headerText !== 'Precio') {
                  const cellContent = rowCells[i].querySelector('span').textContent;
                  rowCells[i].innerHTML = cellContent;
                }
              }
              this.textContent = 'Editar';

              // Restaurar la imagen
              const imgCell = rowCells[2];
              const img = document.createElement('img');
              img.src = imgCell.dataset.src;
              img.alt = rowData.nombre;
              img.style.maxWidth = '150px';
              img.style.maxHeight = '150px';
              imgCell.innerHTML = '';
              imgCell.appendChild(img);

              // Obtener los datos actualizados del tbody
              const updatedData = [];
              const rows = tbody.querySelectorAll('tr');
              rows.forEach(row => {
                const rowData = {};
                const cells = row.querySelectorAll('td');
                cells.forEach((cell, index) => {
                  const headerText = headers[index];
                  const cellText = cell.textContent;
                  rowData[headerText] = cellText;
                });
                updatedData.push(rowData);
              });

              // Llamar al archivo PHP "editar_productos.php" y enviar los datos actualizados
              const xhrEdit = new XMLHttpRequest();
              xhrEdit.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {
                  // Realizar cualquier otra acción necesaria después de guardar los datos
                }
              };

              xhrEdit.open('POST', '../php/editar_productos.php', true);
              xhrEdit.setRequestHeader('Content-Type', 'application/json');
              xhrEdit.send(JSON.stringify(updatedData));
            }
          });

          editButtonCell.appendChild(editButton);
          row.appendChild(editButtonCell);
          tbody.appendChild(row);
        });
        table.appendChild(thead);
        table.appendChild(tbody);
        resultados_productos.appendChild(table);
      } else {
        resultados_productos.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };
  xhr.open('POST', form.action, true);
  xhr.send(formData);
}

