function postData() {
  const form = document.querySelector('#form_buqueda');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    console.log('readyState:', this.readyState);
    console.log('status:', this.status);

    if (this.readyState === 4 && this.status === 200) {
      const resultados_productos = document.getElementById('resultados_clientes');
      resultados_productos.innerHTML = '';
      const data = JSON.parse(this.responseText);

      if (data.length > 0) {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['id cliente', 'Nombre', 'apellido', 'dni', 'cuit', 'localidad', 'direccion', 'correo', 'telefono'];

        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });

        thead.appendChild(headerRow);

        data.forEach(rowData => {
          const row = document.createElement('tr');

          Object.entries(rowData).forEach(([key, value]) => {
            const cell = document.createElement('td');
            if (key === 'id cliente') {
              const textNode = document.createTextNode(value);
              cell.appendChild(textNode);
            } else {
              const span = document.createElement('span');
              span.textContent = value;
              cell.appendChild(span);
            }
            row.appendChild(cell);
          });

          const editButtonCell = document.createElement('td');
          const editButton = document.createElement('button');
          editButton.textContent = 'Editar';
          editButton.addEventListener('click', function() {
            const rowCells = this.parentNode.parentNode.childNodes;

            if (this.textContent === 'Editar') {
              for (let i = 1; i < rowCells.length - 1; i++) {
                const cellContent = rowCells[i].querySelector('span').textContent;
                rowCells[i].innerHTML = `<span contenteditable="true">${cellContent}</span>`;
              }
              this.textContent = 'Guardar';
            } else {
              for (let i = 1; i < rowCells.length - 1; i++) {
                const cellContent = rowCells[i].querySelector('span').textContent;
                rowCells[i].innerHTML = cellContent;
              }
              this.textContent = 'Editar';

              // Obtener los datos actualizados del tbody
              const updatedData = [];
              const rows = tbody.querySelectorAll('tr');
              rows.forEach(row => {
                const rowData = {};
                const cells = row.querySelectorAll('td');
                cells.forEach((cell, index) => {
                  const headerText = headers[index];
                  const cellText = cell.textContent;
                  rowData[headerText] = cellText;
                });
                updatedData.push(rowData);
              });

              // Llamar al archivo PHP "editar_clientes.php" y enviar los datos actualizados
              const xhrEdit = new XMLHttpRequest();
              xhrEdit.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {
                  // Realizar cualquier otra acción necesaria después de guardar los datos

                  // Volver a llamar a postData() para actualizar los resultados
                  postData();
                }
              };

              xhrEdit.open('POST', '../php/editar_clientes.php', true);
              xhrEdit.setRequestHeader('Content-Type', 'application/json');
              xhrEdit.send(JSON.stringify(updatedData));
            }
          });

          editButtonCell.appendChild(editButton);
          row.appendChild(editButtonCell);
          tbody.appendChild(row);
        });

        table.appendChild(thead);
        table.appendChild(tbody);
        resultados_productos.appendChild(table);
      } else {
        resultados_productos.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };

  xhr.open('POST', form.action, true);
  xhr.send(formData);
}

$(document).ready(function() {
  // Capturar el evento de envío del formulario
  $("#form_in_clientes").submit(function(event) {
    event.preventDefault(); // Evitar el envío del formulario

    // Obtener los datos del formulario
    var formData = $(this).serialize();

    // Enviar los datos al servidor mediante AJAX
    $.ajax({
      url: "../php/data_in_clientes.php",
      type: "POST",
      data: formData,
      success: function(response) {
        // Procesar la respuesta del servidor
        console.log(response);
        // Realizar otras operaciones necesarias

        // Por ejemplo, mostrar un mensaje de éxito
        alert("cliente añadido correctamente");

        $("#form_in_clientes")[0].reset();

        // Volver a llamar a postData() para actualizar los resultados
        postData();
      },
            error: function(xhr, status, error) {
            }
        });
    });
});

