<?php
  // Conexión a la base de datos
  $servername = "db";
  $username = "root";
  $password = "test";
  $dbname = "EMPRESA";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  if(isset($_POST['apellido_cliente'])){
    $apellido_cliente = $_POST['apellido_cliente'];
  
      $sql = "SELECT CLIENTES.id_cliente, CLIENTES.nombre, CLIENTES.apellido, CLIENTES.dni, CLIENTES.cuit, CLIENTES.localidad, CLIENTES.direccion, CLIENTES.correo, CLIENTES.telefono
              FROM CLIENTES
              WHERE CLIENTES.apellido like '%$apellido_cliente%'";


      $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
      $results_array = array();
      while($row = $result->fetch_assoc()) {
        $results_array[] = array(
          "id_cliente"=>$row["id_cliente"],
          "nombre"=>$row["nombre"],
          "apellido"=>$row["apellido"],
          "dni"=>$row["dni"],
          "cuit"=>$row["cuit"],
          "localidad"=>$row["localidad"],
          "direccion"=>$row["direccion"],
          "correo"=>$row["correo"],
          "telefono"=>$row["telefono"]
      );
      }
     
      // Enviar los resultados como respuesta en formato JSON
      header('Content-Type: application/json');
      echo json_encode($results_array);
    } else {
      echo "No se encontraron resultados para la fecha seleccionada.";
    }
  }

  // Cerrar la conexión a la base de datos
  $conn->close();
?>
