<?php
  // Conexión a la base de datos
  $servername = "db";
  $username = "root";
  $password = "test";
  $dbname = "EMPRESA";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }else{

  
      $sql = "SELECT DOLAR.dolar
              FROM DOLAR";
      $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
      // Almacenar los resultados en un array
      $results_array = array();
      while($row = $result->fetch_assoc()) {
        $results_array[] = array(
          "dolar"=> "$" .$row["dolar"],
      );
      }
     
      // Enviar los resultados como respuesta en formato JSON
      header('Content-Type: application/json');
      echo json_encode($results_array);
    } else {
    }
  }
  // Cerrar la conexión a la base de datos
  $conn->close();
?>
