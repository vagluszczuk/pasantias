<?php
$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Consulta para obtener los datos de la base de datos
$consulta = "SELECT DISTINCT VENDEDORES.nombre, VENDEDORES.apellido, VENDEDORES.id_vendedor FROM VENDEDORES";
$resultado = mysqli_query($conn, $consulta);

// Crear un array para almacenar los datos
$datos = array();

while ($fila = mysqli_fetch_assoc($resultado)) {
    $nombre = $fila['nombre'];
    $apellido = $fila['apellido'];
    $id_vendedor = $fila['id_vendedor'];
    $datos[] = array('nombre' => $nombre, 'apellido' => $apellido, 'id_vendedor' => $id_vendedor);
}

// Cerrar la conexión
mysqli_close($conn);

// Enviar los datos en formato JSON
header('Content-Type: application/json');
echo json_encode($datos);
?>
