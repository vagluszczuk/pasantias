<?php

$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Consulta para obtener los datos de la base de datos
$consulta = "SELECT DISTINCT COLUMN_NAME
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_NAME = 'PRODUCTOS'
                AND COLUMN_NAME NOT IN ('disponibilidad', 'precio_usd', 'descripcion','imagenes')";
$resultado = mysqli_query($conn, $consulta);

// Crear un array para almacenar los datos
$datos = array();



// Recorrer los resultados y agregarlos al array
while ($fila = mysqli_fetch_assoc($resultado)) {
    $datos[] = $fila;
}

// Cerrar la conexión
mysqli_close($conn);

// Enviar los datos en formato JSON
header('Content-Type: application/json');
echo json_encode($datos);
?>
