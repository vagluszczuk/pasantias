<?php
  // Conexión a la base de datos
  $servername = "db";
  $username = "root";
  $password = "test";
  $dbname = "EMPRESA";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }


  $fecha_1 = $_POST['fecha_1'];

    $sql = "SELECT VENTAS.id_cliente, VENTAS.id_vendedor, VENTAS.fecha_hora, VENTAS.factura, VENTAS.id_venta, CLIENTES.nombre AS nombre_cliente, CLIENTES.apellido AS apellido_cliente, VENDEDORES.nombre AS nombre_vendedor, VENDEDORES.apellido AS apellido_vendedor
            FROM VENTAS
            INNER JOIN CLIENTES ON VENTAS.id_cliente = CLIENTES.id_cliente
            INNER JOIN VENDEDORES ON VENTAS.id_vendedor = VENDEDORES.id_vendedor
            WHERE VENTAS.fecha_hora LIKE '%$fecha_1%'";
  
  $result = $conn->query($sql);
  
  if ($result->num_rows > 0) {
    $results_array1 = array();
    while ($row = $result->fetch_assoc()) {
      $results_array1[] = array(
        "id_venta" => $row["id_venta"],
        "nombre_cliente" => $row["nombre_cliente"] . " " . $row["apellido_cliente"],
        "nombre_vendedor" => $row["nombre_vendedor"] . " " . $row["apellido_vendedor"],
        "fecha_hora" => $row["fecha_hora"]
      );
    }
  }
  echo json_encode($results_array1);

  $conn->close();
?>
