<?php
// Obtener los datos enviados desde JavaScript
$data = json_decode(file_get_contents('php://input'), true);

// Realizar la conexión a la base de datos
$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error al conectar con la base de datos: " . $conn->connect_error);
}

// Recorrer los datos actualizados y ejecutar las consultas para modificar la base de datos
foreach ($data as $row) {
    $idCliente = $row['id vendedor'];
    $nombre = $row['Nombre'];
    $apellido = $row['apellido'];
    $dni = $row['dni'];
    $localidad = $row['localidad'];
    $direccion = $row['direccion'];
    $correo = $row['correo'];
    $telefono = $row['telefono'];

    $sql = "UPDATE VENDEDORES SET nombre='$nombre', apellido='$apellido', dni_vendedor='$dni', localidad='$localidad', domicilio='$direccion', correo='$correo', telefono='$telefono' WHERE id_vendedor='$idCliente'";
    if ($conn->query($sql) !== TRUE) {
        echo "Error al actualizar los datos: " . $conn->error;
    }
}

// Cerrar la conexión a la base de datos
$conn->close();

// Enviar una respuesta al cliente (puedes enviar un mensaje de éxito o cualquier otra información relevante)
echo "Los datos se han actualizado correctamente";
?>
