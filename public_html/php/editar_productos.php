<?php
// Recibir los datos actualizados desde JavaScript
$data = json_decode(file_get_contents('php://input'), true);

// Conectar a la base de datos
$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die('Error de conexión: ' . $conn->connect_error);
}

// Iterar sobre los datos actualizados y actualizar la base de datos
foreach ($data as $rowData) {
    $codigo = $rowData['Codigo'];
    $nombre = $rowData['Nombre'];
    $imagen = $rowData['Imagen'];
    $precio = $rowData['Precio'];
    $rubro = $rowData['Rubro'];
    $descripcion = $rowData['Descripcion'];
    $stock = $rowData['Stock'];
    $disponibilidad = $rowData['Disponibilidad'];

    $sql = "UPDATE PRODUCTOS SET nombre = '$nombre', rubro = '$rubro', descripcion = '$descripcion', stock = '$stock', disponibilidad = '$disponibilidad' WHERE cod_producto = '$codigo'";
    $result = $conn->query($sql);
}

$conn->close();

// Devolver una respuesta (opcional)
$response = [
    'status' => 'success',
    'message' => 'Los datos se han actualizado correctamente.'
];
echo json_encode($response);
?>
