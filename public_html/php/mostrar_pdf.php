<?php
$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error al conectar con la base de datos: " . $conn->connect_error);
}

$id_venta = $_GET['id_venta'];

$sqlSelect = "SELECT factura FROM VENTAS WHERE id_venta = '$id_venta'";
$result = mysqli_query($conn, $sqlSelect);

if ($result && mysqli_num_rows($result) > 0) {
  $row = mysqli_fetch_assoc($result);
  $pdfContentEscaped = $row['factura'];
} else {
  die("No se encontró la factura correspondiente a la ID de venta proporcionada.");
}

header('Content-type: application/pdf');
echo $pdfContentEscaped;

$conn->close();
?>
