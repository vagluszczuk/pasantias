<?php
$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
  }

// Obtener el valor seleccionado del <select>
$producto = $_POST['producto'];

$consulta = "SELECT PRODUCTOS.stock FROM PRODUCTOS where PRODUCTOS.nombre like '%$producto%'";
$resultado = mysqli_query($conn, $consulta);

if ($resultado) {
    $row = $resultado->fetch_assoc();
    $stock = $row['stock'];
  
    // Devolver el número en formato JSON
    echo json_encode(['stock' => $stock]);
  } else {
    // Si no se encuentra el producto, devolver un valor predeterminado
    echo json_encode(['stock' => 0]);
  }
  
?>
