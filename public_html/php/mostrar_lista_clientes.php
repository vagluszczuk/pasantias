<?php
$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Consulta para obtener los datos de la base de datos
$consulta = "SELECT DISTINCT CLIENTES.nombre, CLIENTES.apellido, CLIENTES.id_cliente FROM CLIENTES";
$resultado = mysqli_query($conn, $consulta);

// Crear un array para almacenar los datos
$datos = array();

while ($fila = mysqli_fetch_assoc($resultado)) {
    $nombre = $fila['nombre'];
    $apellido = $fila['apellido'];
    $id_cliente = $fila['id_cliente'];
    $datos[] = array('nombre' => $nombre, 'apellido' => $apellido, 'id_cliente' => $id_cliente);
}

// Cerrar la conexión
mysqli_close($conn);

// Enviar los datos en formato JSON
header('Content-Type: application/json');
echo json_encode($datos);
?>
