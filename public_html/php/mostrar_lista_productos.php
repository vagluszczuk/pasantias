<?php

$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Consulta para obtener los datos de la base de datos
$consulta = "SELECT DISTINCT PRODUCTOS.nombre,PRODUCTOS.cod_producto,PRODUCTOS.stock FROM PRODUCTOS where PRODUCTOS.disponibilidad like '%si%'";
$resultado = mysqli_query($conn, $consulta);

// Crear un array para almacenar los datos
$datos = array();

// Recorrer los resultados y agregarlos al array
while ($fila = mysqli_fetch_assoc($resultado)) {
    $nombreCodigo = $fila['nombre'];
    $cod_producto = $fila['cod_producto'];
    $stock = $fila['stock'];
    $datos[] = array('nombreCodigo' => $nombreCodigo, 'cod_producto' => $cod_producto, 'stock' => $stock);
}

// Cerrar la conexión
mysqli_close($conn);

// Enviar los datos en formato JSON
header('Content-Type: application/json');
echo json_encode($datos);
?>
