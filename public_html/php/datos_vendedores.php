<?php
  // Conexión a la base de datos
  $servername = "db";
  $username = "root";
  $password = "test";
  $dbname = "EMPRESA";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  if(isset($_POST['dni_vendedor'])){
    $dni_vendedor = $_POST['dni_vendedor'];
  
      $sql = "SELECT VENDEDORES.id_vendedor, VENDEDORES.nombre, VENDEDORES.apellido, VENDEDORES.dni_vendedor, VENDEDORES.localidad, VENDEDORES.domicilio, VENDEDORES.correo, VENDEDORES.telefono
              FROM VENDEDORES
              WHERE VENDEDORES.dni_vendedor like '%$dni_vendedor%'";


      $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
      // Almacenar los resultados en un array
      $results_array = array();
      while($row = $result->fetch_assoc()) {
        $results_array[] = array(
          "id_vendedor"=>$row["id_vendedor"],
          "nombre"=>$row["nombre"],
          "apellido" => $row["apellido"],
          "dni_vendedor"=>$row["dni_vendedor"],
          "correo"=>$row["correo"],
          "telefono"=>$row["telefono"],
          "localidad"=>$row["localidad"],
          "domicilio"=>$row["domicilio"]
      );
      }
     
      // Enviar los resultados como respuesta en formato JSON
      header('Content-Type: application/json');
      echo json_encode($results_array);
    } else {
      header('Content-Type: application/json');
      echo json_encode("No se encontraron resultados para la fecha seleccionada.");
    }
  }
  // Cerrar la conexión a la base de datos
  $conn->close();
?>
