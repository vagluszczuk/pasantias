<?php

// Conexión a la base de datos
$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$descu=0;
// Obtener el valor del contador
$contador = $_POST['contador'];
$id_cliente = $_POST['opciones_cliente'];
$id_vendedor = $_POST['opciones_vendedor'];
$descu =+ $_POST['descuento'];
// Obtener el array de datos del formulario
$datosArray = $_POST['datosArray'];


$sql1 = "SELECT VENDEDORES.nombre,VENDEDORES.apellido,VENDEDORES.telefono  FROM VENDEDORES where VENDEDORES.id_vendedor = $id_vendedor";
$resultado1 = mysqli_query($conn, $sql1);

if ($resultado1) {
    $res1 = mysqli_fetch_assoc($resultado1);
    $nombre_vendedor = $res1['nombre'];
    $apellido_vendedor = $res1['apellido'];
}

$sql2 = "SELECT CLIENTES.nombre,CLIENTES.apellido,CLIENTES.localidad,CLIENTES.direccion,CLIENTES.cuit,CLIENTES.telefono FROM CLIENTES where CLIENTES.id_cliente = $id_cliente";
$resultado2 = mysqli_query($conn, $sql2);


if ($resultado2) {
    $res2 = mysqli_fetch_assoc($resultado2);
    $nombre_cliente = $res2['nombre'];
    $apellido_cliente = $res2['apellido'];
    $localidad_cliente = $res2['localidad'];
    $direccion_cliente = $res2['direccion'];
    $cuit_cliente = $res2['cuit'];
    $telefono_cliente = $res1['telefono'];
}

$sql3 = "SELECT MAX(VENTAS.id_venta) as max_id FROM VENTAS;";
$resultado3 = mysqli_query($conn, $sql3);

if ($resultado3) {
    $res3 = mysqli_fetch_assoc($resultado3);
    $id_venta += 1 + $res3['max_id'];
    $id_venta_formatted = sprintf('%08d', $id_venta - 1);
}


// Decodificar el array de datos
$datosArrayDecodificado = json_decode($datosArray, true);

require_once('tcpdf/tcpdf.php');

// Datos del emisor de la factura
$emisorNombre = "Dimayer";
$emisorDomicilio = "Araguaya 546. Llavallol";
$emisorCUIT = "11-11111111-1";
$emisortelefono = "11 6049 1352";
$emisorCondicionIVA = "Responsable Inscripto";
// $emisorCAI = "12345678901234"; // Número de CAI asignado por la AFIP

// Datos del vendedor de la factura
$vendedorNombre = $nombre_vendedor." ".$apellido_vendedor;


// Datos del receptor de la factura
$receptorNombre = $nombre_cliente." ".$apellido_cliente;
$receptorDomicilio = $direccion_cliente;
$receptorCUIT = $cuit_cliente;

// Numeración de la factura
$numeroFactura = $id_venta_formatted; // Número de factura único y consecutivo asignado

// Crear nuevo objeto PDF
$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8');

// Establecer información del documento
$pdf->SetCreator($emisorNombre);
$pdf->SetAuthor($emisorNombre);
$pdf->SetTitle('Factura de pago');
$pdf->SetSubject('Factura de pago');
$pdf->SetKeywords('Factura, Pago, AFIP');

// Habilitar encabezado y pie de página
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// Establecer margenes
$pdf->SetMargins(15, 15, 15);

// Agregar una nueva página
$pdf->AddPage();

// Definir el porcentaje deseado
$porcentajeAncho = 50; // 50% del ancho total

// Obtener el ancho total de la página en milímetros
$anchoTotalMM = $pdf->GetPageWidth();

// Calcular el ancho en milímetros
$anchoCeldaMM = ($porcentajeAncho / 100) * $anchoTotalMM;

// Establecer el contenido de la factura aquí

$pdf->SetFont('helvetica', 'B', 14);
$pdf->Cell(0, 10, 'Remito de pago', 0, 1, 'C');

$pdf->SetFont('helvetica', '', 10);
$pdf->Cell($anchoCeldaMM, 10, 'Fecha: '.date('Y-m-d'), 0, 0,'L');
$pdf->Cell(0, 10, 'Número de factura: '.$numeroFactura, 0, 1,'R');

$pdf->Cell(0, 0, "___________________________________________________________________________________________", 0, 1,);
$pdf->Cell(0, 5, "", 0, 1,);

$pdf->Cell($anchoCeldaMM, 5, 'Emisor: '.$emisorNombre, 0, 0,'L');
$pdf->Cell(0, 5, 'Vendedor: '.$vendedorNombre, 0, 1,'L');
$pdf->Cell($anchoCeldaMM, 5, 'Domicilio: '.$emisorDomicilio, 0, 1,'L');
$pdf->Cell(0, 5, 'Telefono: '.$emisortelefono, 0, 1,'L');
$pdf->Cell(0, 5, 'CUIT: '.$emisorCUIT, 0, 1);
$pdf->Cell(0, 5, 'Condición frente al IVA: '.$emisorCondicionIVA, 0, 1);

$pdf->Cell(0, 5, "", 0, 1,);

$pdf->Cell(0, 5, 'Receptor: '.$receptorNombre, 0, 1);
$pdf->Cell(0, 5, 'Domicilio: '.$receptorDomicilio, 0, 1);
$pdf->Cell(0, 5, 'CUIT: '.$receptorCUIT, 0, 1);
$pdf->Cell(0, 5, 'Telefono: '.$telefono_cliente, 0, 1,'L');


$pdf->Cell(0, 0, "___________________________________________________________________________________________", 0, 1,);
$pdf->Cell(0, 5, "", 0, 1,);

$pdf->SetFont('helvetica', 'B', 10);
$pdf->Cell(30, 8, ' Producto', 0, 0, 'L');
$pdf->Cell(30, 8, 'Precio', 0, 0, 'L');
$pdf->Cell(70, 8, 'Cantidad', 0, 0, 'L');
$pdf->Cell(49, 8, 'Total', 0, 1, 'R');



$total = 0;
// Mostrar los datos del formulario
foreach ($datosArrayDecodificado as $dato) {
    $producto = $dato['producto'];
    $cantidad = $dato['cantidad'];
    $precio = $dato['precio'];
    $subtotal = $dato['subtotal'];

    // Convertir $subtotal a un número flotante
    $subtotal = floatval($subtotal);

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(30, 8, " ".$producto, 0, 0, 'L');
    $pdf->Cell(30, 8, "$".$precio, 0, 0, 'L');
    $pdf->Cell(70, 8, $cantidad, 0, 0, 'L');
    $pdf->Cell(49, 8, "$".$subtotal, 0, 1, 'R');

    $total += $subtotal;
}

$pdf->Ln(20);



// Calcular totales

$descu_tot = $total/100*$descu;
$subtot1 = $total - $descu_tot;
$iva = $subtot1 * 0.21;
$totaltotal = $subtot1 + $iva;

$pdf->SetFont('helvetica', 'B', 10);

// Crear tabla de totales
$pdf->SetFont('helvetica', 'B', 10);

$pdf->Cell(30, 8, 'Subtotal inicial', 'L,T', 0, 'L');
$pdf->Cell(30, 8, '$'.$total, 'T,R', 1, 'R');

$pdf->Cell(30, 8, 'Descuento: '.$descu."%", 'L', 0, 'L');
$pdf->Cell(30, 8, '$'.$descu_tot, 'R', 1, 'R');

$pdf->Cell(30, 8, 'Subtotal', 'L', 0, 'L');
$pdf->Cell(30, 8, '$'.$subtot1, 'R', 1, 'R');

$pdf->Cell(30, 8, 'IVA (21%)', 'L', 0, 'L');
$pdf->Cell(30, 8, '$'.$iva, 'R', 1, 'R');

$pdf->Cell(60, 8, '', 'R,L', 1,);

$pdf->Cell(30, 8, 'Total', 'L,B', 0, 'L');
$pdf->Cell(30, 8, '$'.$totaltotal, 'R,B', 1, 'R');

$pdf->Output('factura.pdf', 'I'); 

// Guardar el contenido del archivo PDF en la base de datos
$pdfContent = $pdf->Output('factura.pdf', 'S'); // Obtiene el contenido del PDF como una cadena de bytes

// Escapar caracteres especiales en el contenido del archivo
$pdfContentEscaped = mysqli_real_escape_string($conn, $pdfContent);

// Insertar el contenido del archivo PDF en la base de datos
$sqlInsert = "INSERT INTO VENTAS (id_venta, id_cliente, id_vendedor, nombre_factura, factura, fecha_hora) VALUES ('$id_venta', '$id_cliente', '$id_vendedor', 'factura.pdf', '$pdfContentEscaped', NOW())";
mysqli_query($conn, $sqlInsert);

foreach ($datosArrayDecodificado as $dato) {
    $producto = $dato['producto'];
    $cantidad = $dato['cantidad'];
    
$sqlInsert1 = "UPDATE PRODUCTOS SET PRODUCTOS.stock = PRODUCTOS.stock-'$cantidad' where PRODUCTOS.nombre = '$producto'";
mysqli_query($conn, $sqlInsert1);
}

$conn->close();
?>
