<?php
    // Conexión a la base de datos
    $servername = "db";
    $username = "root";
    $password = "test";
    $dbname = "EMPRESA";

    $conn = mysqli_connect($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $usd = $_POST["usd"];
        $porcentaje = $_POST["porcentaje"];
        $cod_producto = $_POST["cod_producto"];
        $nuevo_precio_usd = $_POST["nuevo_precio_usd"];

        if (!empty($usd)) {
            
            $sql1 = "SELECT COUNT(*) AS cantidad_productos FROM PRODUCTOS";
            $resultado1 = mysqli_query($conn, $sql1);
            $sql11="UPDATE DOLAR SET dolar = '$usd'";
            $resultado11 = mysqli_query($conn, $sql11);

            

            if ($resultado1) {
                $cant = mysqli_fetch_assoc($resultado1);
                $cant_productos = $cant['cantidad_productos'];
            }
            if ($resultado11) {
            }

            for($i = 1;$i <= $cant_productos;$i++){

                $sql2 = "SELECT precio_usd from PRODUCTOS WHERE cod_producto = '$i'";
                $resultado2 = mysqli_query($conn, $sql2);

                if ($resultado2) {
                    $valor_usd = mysqli_fetch_assoc($resultado2);
                    $precio_usd = $valor_usd['precio_usd'];
                }

                $nuevo_precio = $usd*$precio_usd;
                $verdadero_nuevo_precio = round($nuevo_precio,-1);

                $sql3="UPDATE PRODUCTOS SET precio = '$verdadero_nuevo_precio' WHERE cod_producto = '$i'";
                $resultado3 = mysqli_query($conn, $sql3);

                if ($resultado3){
                    echo "El precio del producto se actualizó correctamente.";
                }
            }
        }
        
        if(!empty($porcentaje)){
            
            $sql4 = "SELECT COUNT(*) AS cantidad_productos FROM PRODUCTOS";
            $resultado4 = mysqli_query($conn, $sql4);


            if ($resultado4) {
                $cant = mysqli_fetch_assoc($resultado4);
                $cant_productos = $cant['cantidad_productos'];
            }

            for($i = 1;$i <= $cant_productos;$i++){
                
                $sql5 = "SELECT precio_usd from PRODUCTOS WHERE cod_producto = '$i'";
                $resultado5 = mysqli_query($conn, $sql5);

                if ($resultado5) {
                    $valor_usd = mysqli_fetch_assoc($resultado5);
                    $precio_usd = $valor_usd['precio_usd'];
                }

                $sql6 = "SELECT precio from PRODUCTOS WHERE cod_producto = '$i'";
                $resultado6 = mysqli_query($conn, $sql6);

                if ($resultado6) {
                    $valor = mysqli_fetch_assoc($resultado6);
                    $precio = $valor['precio'];
                }

                $aumento = $precio * ($porcentaje/100) ;
                $aumento_usd = $precio_usd * ($porcentaje/100) ;
                
                $sql7="UPDATE PRODUCTOS SET precio = precio + '$aumento' WHERE cod_producto = '$i'";
                $sql8="UPDATE PRODUCTOS SET precio_usd = precio_usd + '$aumento_usd' WHERE cod_producto = '$i'";
                $resultado7 = mysqli_query($conn, $sql7);
                $resultado8 = mysqli_query($conn, $sql8);
                
                if ($resultado7){
                    echo "El precio del producto '$i' se actualizó correctamente.";
                }
                if ($resultado8){
                    echo "El precio en usd del producto '$i' se actualizó correctamente.";
                }

            }
        }
        if(!empty($cod_producto) && !empty($nuevo_precio_usd)){

            $sql12="SELECT DOLAR.dolar from DOLAR";
            $resultado12 = mysqli_query($conn, $sql12);

            if ($resultado12) {
                $aux1 = mysqli_fetch_assoc($resultado12);
                $precio_del_dolar = $aux1['dolar'];
            }

            $nuevo_precio_pesos = $precio_del_dolar*$nuevo_precio_usd ;

            $sql9="UPDATE PRODUCTOS SET precio = '$nuevo_precio_pesos' WHERE cod_producto = '$cod_producto'";
            $sql10="UPDATE PRODUCTOS SET precio_usd = '$nuevo_precio_usd' WHERE cod_producto = '$cod_producto'";
            $resultado9 = mysqli_query($conn, $sql9);
            $resultado10 = mysqli_query($conn, $sql10);
            
            if (!$resultado9){
                http_response_code(500); // Código de error 500 para indicar un error en el servidor
            }
            if (!$resultado10){
                http_response_code(500); // Código de error 500 para indicar un error en el servidor
            }
        }
    }
    $conn->close();
?>
