<?php
// Conexión a la base de datos
$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
  }


// Consulta para obtener el stock de todos los productos
$sql = "SELECT nombre, stock FROM PRODUCTOS where PRODUCTOS.disponibilidad = 'si' or 'SI' or 'sI' or'Si'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Crear un array para almacenar los datos
    $stockArray = array();

    // Recorrer los resultados de la consulta y agregarlos al array
    while ($row = $result->fetch_assoc()) {
        $stockArray[] = array(
            "nombre" => $row["nombre"],
            "stock" => $row["stock"]
        );
    }

    // Convertir el array a formato JSON
    $jsonStock = json_encode($stockArray);

    // Establecer la cabecera de respuesta como JSON
    header('Content-Type: application/json');

    // Enviar el JSON como respuesta
    echo $jsonStock;
} else {
    echo "No se encontraron productos en la base de datos.";
}

$sql1 = "SELECT COUNT(*) AS cantiad FROM `PRODUCTOS`";
$result1 = $conn->query($sql1);

if ($result1) {
    $aux1 = mysqli_fetch_assoc($result1);
    $cant_productos = $aux1['cantiad'];
}

for($i=1;$i<=$cant_productos;$i++){
    $sql2 = "SELECT stock FROM `PRODUCTOS` where PRODUCTOS.cod_producto = '$i'";
    $result2 = $conn->query($sql2);

    if ($result2) {
        $aux2 = mysqli_fetch_assoc($result2);
        $stock = $aux2['stock'];
    }

    if($stock==0){
        $sql3 = "UPDATE PRODUCTOS SET disponibilidad = 'no' WHERE cod_producto = '$i'";
        $result3 = $conn->query($sql3);
    }
}

$conn->close();
?>
