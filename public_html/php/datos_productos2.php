<?php
  // Conexión a la base de datos
  $servername = "db";
  $username = "root";
  $password = "test";
  $dbname = "EMPRESA";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  if(isset($_POST['opciones_curso'])){
    $opciones_curso = $_POST['opciones_curso'];
  

      $sql = "SELECT PRODUCTOS.cod_producto, PRODUCTOS.nombre, PRODUCTOS.imagenes, PRODUCTOS.precio, PRODUCTOS.rubro, PRODUCTOS.descripcion, PRODUCTOS.stock, PRODUCTOS.disponibilidad
              FROM PRODUCTOS
              ORDER BY `PRODUCTOS`.`$opciones_curso` ASC;";



      $result = $conn->query($sql);
    
      if ($result->num_rows > 0) {
        $results_array = array();
        while ($row = $result->fetch_assoc()) {
          $imageData = base64_encode($row['imagenes']);
          $imageUrl = 'data:image/jpg;base64,' . $imageData;
          $results_array[] = array(
            "cod_producto" => $row["cod_producto"],
            "nombre" => $row["nombre"],
            "imagen" => $imageUrl, // Agrega la URL codificada base64 de la imagen
            "precio" => "$" . $row["precio"],
            "rubro" => $row["rubro"],
            "descripcion" => $row["descripcion"],
            "stock" => $row["stock"],
            "disponibilidad" => $row["disponibilidad"]
          );
        }
     
      // Enviar los resultados como respuesta en formato JSON
      header('Content-Type: application/json');
      echo json_encode($results_array);
    } else {
      echo "No se encontraron resultados para la fecha seleccionada.";
    }
  }

  // Cerrar la conexión a la base de datos
  $conn->close();
?>
