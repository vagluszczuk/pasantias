<?php
// Conexión a la base de datos
$servername = "db";
$username = "root";
$password = "test";
$dbname = "EMPRESA";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST['cod_producto'])) {
  $cod_producto = $_POST['cod_producto'];

  $sql = "SELECT PRODUCTOS.cod_producto, PRODUCTOS.nombre, PRODUCTOS.imagenes, PRODUCTOS.precio, PRODUCTOS.rubro, PRODUCTOS.descripcion, PRODUCTOS.stock, PRODUCTOS.disponibilidad
          FROM PRODUCTOS
          WHERE PRODUCTOS.cod_producto = '$cod_producto'";

  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    $results_array = array();
    while ($row = $result->fetch_assoc()) {
      $imageData = base64_encode($row['imagenes']);
      $imageUrl = 'data:image/jpg;base64,' . $imageData;
      $results_array[] = array(
        "nombre" => $row["nombre"],
        "imagen" => $imageUrl, // Agrega la URL codificada base64 de la imagen
        "precio" => "$" . $row["precio"]
      );
    }
  
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($results_array);
  } else {
    echo json_encode("No se encontraron resultados para la fecha seleccionada.");
  }
  
}

// Cerrar la conexión a la base de datos
$conn->close();
?>
